package util

import model.AppConfig


object ConfigUtil {
  def getConfig(args: Array[String]): AppConfig = {
    args.length match {
      case 0 => AppConfig(None, None, None, logEnabled = false)
      case 1 => AppConfig(Option(args(0)), None, None, logEnabled = false)
      case 2 => AppConfig(Option(args(0)), Option(args(1)), None, logEnabled = false)
      case 3 => AppConfig(Option(args(0)), Option(args(1)), Option(args(2).toInt), logEnabled = false)
      case default => AppConfig(Option(args(0)), Option(args(1)), Option(args(2).toInt), logEnabled = args(3).toBoolean)
    }
  }
}
