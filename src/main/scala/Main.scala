
import java.io.{BufferedWriter, File, FileWriter}
import java.net.URLDecoder
import java.util.concurrent.Executors

import model.{Empty, SiteMetaData}
import monix.eval.Task
import monix.execution.Scheduler
import monix.execution.schedulers.SchedulerService
import service.TaskFabric
import util.ConfigUtil

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.Source

object Main extends App {
  val timeout = 1000000.seconds
  val config = ConfigUtil.getConfig(args)

  implicit val scheduler: SchedulerService = {
    val javaService = Executors.newFixedThreadPool(config.threadCount.getOrElse(4))
    Scheduler(javaService)
  }
  //Delete previous result file
  new File(config.outputFile.getOrElse("/Users/maxfedosov/Downloads/result.txt")).delete()

  val bw = new BufferedWriter(new FileWriter(config.outputFile.getOrElse("/Users/maxfedosov/Downloads/result.txt")))

  val taskService = new TaskFabric(true)
  val source = Source
    .fromFile(config.inputFile.getOrElse("/Users/maxfedosov/Downloads/short.txt"))

  val data = source
    .getLines()
    .map(it => {
      taskService.crawl(it)
    })
    .toSeq

  val aggregate = Task
    .gather(data)
    .map(_.toList)
    .runToFuture

  Await.result(aggregate, timeout)
    .foreach {
      case it: SiteMetaData =>
        bw.write(s"${URLDecoder.decode(it.url, "utf-8")}:" +
          s" {title: ${it.title}," +
          s" metaDescription: ${it.metaDescriptions}," +
          s" metaKeywords: ${it.metaKeywords}")
        bw.newLine()
        bw.newLine()
        bw.newLine()
      case it: Empty =>
        bw.write(s"${it.url}: {errorMessage:  ${it.reason}}")
        bw.newLine()
        bw.newLine()
        bw.newLine()
    }

  scheduler.shutdown()
  source.close()
  bw.close()
}

