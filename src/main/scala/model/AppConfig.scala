package model

case class AppConfig(inputFile: Option[String],
                     outputFile: Option[String],
                     threadCount: Option[Int],
                     logEnabled: Boolean)
