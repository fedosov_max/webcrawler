package model

case class SiteMetaData(url: String,
                        title: String,
                        metaKeywords: List[String],
                        metaDescriptions: List[String]) extends SiteData
