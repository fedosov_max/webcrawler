package service


import java.net.URLDecoder

import model.{Empty, SiteMetaData}
import monix.eval.Task
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.scraper.ContentExtractors.elementList
import org.jsoup.Connection

import scala.concurrent.duration._

class TaskFabric(val logEnabled: Boolean) {


  def crawl(it: String) = Task {
    val browser = new JsoupBrowser {
      override def requestSettings(conn: Connection) = conn.timeout(9000)
    }
    val document = browser.get(s"http://" + URLDecoder.decode(it, "utf-8"))
    val titleDoc = document >> elementList("head")
    val title = titleDoc
      .find(it => it.tagName == "title")
      .map(it => it.text)
      .getOrElse("No title")
    val metaElements = document >> elementList("meta");
    val metaKeywords = metaElements
      .filter(it => it.hasAttr("name") && it.attr("name")
        .toLowerCase
        .contains("description"))
      .map(it => it.attr("content"))
    val metaDescription = metaElements
      .filter(it => it.hasAttr("name") && it.attr("name")
        .toLowerCase
        .contains("keywords"))
      .map(it => it.attr("content"))
    if (logEnabled) println(s"url ${it} procesed")
    SiteMetaData(it, title, metaDescription, metaKeywords)
  }.onErrorRecover { case default =>
    if (logEnabled) println(s"some error ${it}: ${default.getMessage}")
    Empty(it, default.getMessage)
  }
}
