name := "webcrawler"

version := "0.1"

scalaVersion := "2.12.1"
val monixVersion = "3.0.0"
val sttpVersion = "1.6.7"
val circeVersion = "0.11.1"
val scalaScraper = "2.1.0"
libraryDependencies ++= Seq(
  "io.monix" %% "monix" % monixVersion,
  "net.ruippeixotog" %% "scala-scraper" % scalaScraper
)
